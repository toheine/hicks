# Changelog des History-Checkers

## Version: 231011

- Anpassung der Kalenderwochen auf das aktuelle Schuljahr. 
- Fehlerhafte Berechtigungen für die Lehrer-Reports repariert

## Version: 221122

In den User-Verzeichnissen werden jetzt pro Woche fünf Dateien erstellt:

- `commands_Kalenderwoche_total.txt`   > Alle eingegeben Befehle
- `commands_Kalenderwoche_correct.txt` > Alle korrekten Befehle
- `commands_Kalenderwoche_wrong.txt`   > Alle falsche Befehle
- `commands_Kalenderwoche_suniq.txt`   > Alle softuniq-Befehle
- `commands_Kalenderwoche_huniq.txt`   > Alle harduniq-Befehle

Zudem erfolgt eine Auswertung in zwei Dateien:

- `p1_report.csv` > Auswertung für Halbjahr1
- `p2_report.csv` > Auswertung für Halbjahr2

Diese hat beispielsweise folgenden Aufbau:

```
Week;Total;Wrong;Correct;Softuniq;Harduniq
kw46;42;2;40;11;8
kw47;14;1;13;10;9
```

Hier hat der Testschüler in Kalenderwoche 46 insgesamt 42 Befehle eingegeben. Davon waren 2 falsch und 40 korrekt. Es waren insgesamt 11 softuniq- und 8 harduniq-Befehle. Zur besseren Übersicht kann der Inhalt der Datei in einem Tabellenkalulations-Programm geöffnet werden.

## Version: 221017

Optimierung für die Nutzung via crontab. Rückmeldungen befinden sich in Zukunft alle 5 Minuten im Verzeichns ~/command-feedback. Alle Nutzer der Gruppe students erhalten diese Rückmeldung.

Alle Lehrer (Gruppe teachers) bekommen die Student-reports automatisch in ihre Heimatverzeichnis kopiert.

Alle bisher eingegeben Befehle jetzt Wochenweise und nach "correct" und "wrong" enthalten.

## Version: 221013

Erste Rückmeldung an Students durch Auslesen der Datenbank und Generierung von Text-Dateien. Bisher nur ein Report der eingegebenen Befehle. Aufsplittung in Wochen und das Zählen von softuniq- und harduniq-Befehlen folgt.

## Version: 220921

Die Art und Weise wie in den vergangenen Monaten die Befehle der Students aufgenommen wurde, war nicht mehr effizient. Daher läuft der "historychecker" nun etwas anders:

- Das Tool zur Auswertung muss neu erstellt werden.
- Daher liegen aktuell keine User-Reports vor.
- Die Befehle werden bei Eingabe in einer Datenbank gespeichert.

Bei jedem Befehl werden folgende Infos gespeichert:

- Zeitsempel (timestamp)
- Befehl (command)
- Fehlercode (exitcode)
- Aktuelles Verzeichnis (pwd)
- User

In Zukunft werden nur Befehle in die Auswertung mit aufgenommen, die keinen Fehlercode besitzen (exitcode=0).

Code veröffentlicht unter der GPLv3-Lizenz: https://codeberg.org/toheine/hicks
