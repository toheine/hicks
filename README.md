# hicks

With this tool a teacher can check how many (correct) commands are entered by the students. Every command is stored in a database with its exit code.

I use this tool on my learning servers. The students must log in at least once a week to enter prededfined number of different commands. The number of different, correct commands is included in their personal grade. In this way, the students deal with the system on a regular basis and achieve a higher level on the Linux Command Line.

## Caution:

The tool serves a certain didactic purpose and should in no case be implemented on a productive system. This tool is a kind of spyware, because every shell-command is stored in a database. Therefore, the teacher should be transparent about this analysis to the students.

## Requirements on the system

Install mariadb-server. After securing the db-server via `mysql_secure_installation`, create a database to store every command in there:

```
create database hicks;
use hicks;
create table commands ( 
id integer not null auto_increment, 
time integer not null, 
cmd varchar(40) not null, 
exitcode tinyint not null, 
pwd varchar(40) not null, 
user varchar(20) not null, 
primary key(id));
create user ''@'localhost';
grant insert on hicks.commands to ''@'localhost';
flush privileges;
``` 

After that customize the file `/etc/bash.bashrc and append the following lines:

```
prompt_command () {
    exitcode=$?
    if getent group students | grep -q "$USER"; then 
        time="$(date +%s)"
        history -a
        cmd=$(tail -n1 ~/.bash_history)
        cmd=${cmd:0:40}
        cmd=$(echo $cmd | sed "s/'/\"/g")
        dir=${PWD:0:40}
        user=${USER:0:20}
        printf -v sqlstring "insert into hicks.commands  
             (time,cmd,exitcode,pwd,user)
             values 
             ($time,'%s',$exitcode,'$dir','$user');" "$cmd" 
        mysql -e "$sqlstring"
        (exit $exitcode)
    fi
}

shopt -s histappend
HISTFILESIZE=1000000
PROMPT_COMMAND=prompt_command
```

## Add student and teachers on your system

You should create two groups: students and teachers. Every user who is a student should be added to the group students. Do the same with the teacher users. e. g.:

```
useradd -m -s /bin/bash my-crazy-teacher-user
groupadd teachers
usermod -aG teachers my-crazy-teacher-user

useradd -m -s /bin/bash student-user
groupadd students
usermod -aG students student-user
```

## Installation

Install the script in opt:

```
cd opt
git clone https://codeberg.org/toheine/hicks.git
chown -R root:teachers hicks
chmod -R o-rwx hicks
```

## Add a cronjob to call the script every X minutes

The student want a feedback just in time. To make that possible you should add a cronjob via `crontab -e`:
 
```
*/5 * * * * sudo /opt/hicks/hicks >> /opt/hicks/hicks.log 2>&1
```

## Liveview

If you want to present a live view to follow activities on your learn-server, you can do it with a script like the following one:

```shell
#!/bin/bash
while true; do
    clear
    mysql -D hicks -e "select * from commands order by id DESC limit 10;" 
    sleep 2
done

```

## usefull packages for linux essentials

The following packages are also useful for a learning server (installation via apt):

```shell
apt install man info zip bzip2 tree
```


## Recommendations

- Don't use a docker- or lxc-container for a learning-server. Such containers do not fully represent a Linux host. For example, there are not the usual disks or partitions under /dev (e.g., /dev/sda, /dev/sda1, /dev/sda2)
- If you install `fail2ban` to secure your SSH-Logins against brute-force-attacs (which is a very good idea), you should consider to add your schools IP in the `/etc/fail2ban/jail.conf` as an ignored IP. If you do not and your students use your server in the regular lessons, nobody can access the server from your school, if one student enters his password incorrectly more than three times.
- You should add a teststudent to the server to check the function of the script

